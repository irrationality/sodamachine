﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SodaMachine
{
    public class SodaMachine : ISodaMachine
    {
        private const int SodaPrice = 30;
        public Wallet Wallet { get; set; } = new Wallet();
        public Wallet Escrow { get; set; } = new Wallet();
        private int EscrowSum { get; set; }

        public void PushCoin(Nominal coin)
        {
            Escrow[coin] += 1;            
            EscrowSum += (int)coin;
        }
         
        public void ReturnEscrow(Wallet wallet)
        {
            FlushEscrow(wallet);
        }

        public Wallet BuySoda()
        {
            if (EscrowSum < SodaPrice)
                throw new Exception("You have not enough balance for buying soda");

            FlushEscrow(this.Wallet);

            return CountChange();
        }

        private Wallet CountChange()
        {
            var changeSum = EscrowSum - SodaPrice;
            return CountChangeRecursive(changeSum, new Wallet()) ;
        }

        private Wallet CountChangeRecursive(int changeSum, Wallet change) {
            var res = Enum.GetValues(typeof(Nominal)).
                Cast<Nominal>()
                .Select(v=> {
                    var diff = changeSum - (int)v;
                    return diff >= 0 || Wallet[v] == 0 ? 
                    new ChangeDiff{ Nominal = v, Diff = diff } :
                    new ChangeDiff{ Nominal = v, Diff = int.MaxValue };
                }).Min();

            if (res.Diff == int.MaxValue)
                throw new Exception("Oh sorry! But I can't give you change");

            change[res.Nominal] += 1;

            if (res.Diff == 0) return change;

            return CountChangeRecursive(res.Diff, change);
            
        }

        private void FlushEscrow(Wallet wallet)
        {
            wallet[Nominal.Cents] += Escrow[Nominal.Cents];
            wallet[Nominal.Fives] += Escrow[Nominal.Fives];
            wallet[Nominal.Quarters] += Escrow[Nominal.Quarters];
            wallet[Nominal.Halfs] += Escrow[Nominal.Halfs];

            EscrowSum = 0;
            Escrow = new Wallet();
        }
    }
}
