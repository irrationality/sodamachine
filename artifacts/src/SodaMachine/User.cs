﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SodaMachine
{
    public class User 
    {
        public Wallet Wallet { get; set; } = new Wallet();

        public ISodaMachine SodaMachine { get; set; }


        public string PushCoin(Nominal coin)
        {
            if (Wallet[coin] == 0) return string.Format("Ooops! You have no {0}", coin);

            Wallet[coin] -= 1;
            SodaMachine.PushCoin(coin);

            return string.Format("You've pushed {0}", coin);
        }

        public string BuySoda()
        {
            try
            {
                SodaMachine.BuySoda();
                return "Wheeee!!! Soadaaaa!!!!";
            }
            catch (Exception ex) {

                return string.Format("Oh no! SodaMachine says - {0}", ex.Message);
            }
        }


        public string ReturnEscrow() {
            SodaMachine.ReturnEscrow(Wallet);
            return "";
        }
        
    }
}
