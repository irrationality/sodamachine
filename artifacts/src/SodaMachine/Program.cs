﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SodaMachine
{
    public class Program
    {
        public void Main(string[] args)
        {
            var user = new User { SodaMachine = new SodaMachine()};
            ConsoleKeyInfo key;   

            do
            {               
                Console.WriteLine("1. Push cent");
                Console.WriteLine("2. Push five");
                Console.WriteLine("3. Push quarter");
                Console.WriteLine("4. Push Half");
                Console.WriteLine("5. Buy soda");
                Console.WriteLine("6. Return escrow");

                key = Console.ReadKey();
                Console.WriteLine();

                ApplyCommand(key, user);
            } while (key.Key != ConsoleKey.Escape);
        }


        public void ApplyCommand(ConsoleKeyInfo key, User user)
        {
            switch (key.Key) {
                case ConsoleKey.D1:
                    Console.WriteLine(user.PushCoin(Nominal.Cents));
                    break;
                case ConsoleKey.D2:
                    Console.WriteLine(user.PushCoin(Nominal.Fives));
                    break;
                case ConsoleKey.D3:
                    Console.WriteLine(user.PushCoin(Nominal.Quarters));
                    break;
                case ConsoleKey.D4:
                    Console.WriteLine(user.PushCoin(Nominal.Halfs));
                    break;
                case ConsoleKey.D5:
                    Console.WriteLine(user.BuySoda());
                    break;
                case ConsoleKey.D6:
                    Console.WriteLine(user.ReturnEscrow());
                    break;

            }
        }
    }
}
