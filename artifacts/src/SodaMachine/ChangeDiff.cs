﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SodaMachine
{
    public class ChangeDiff : IComparable<ChangeDiff>
    {
        public Nominal Nominal { get; set; }
        public int Diff { get; set; }

        public int CompareTo(ChangeDiff other) {
            return Diff - other.Diff;
        }
    }
}
