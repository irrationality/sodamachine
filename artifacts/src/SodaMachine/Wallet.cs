﻿using System.Collections.Generic;

namespace SodaMachine
{
    public class Wallet : Dictionary<Nominal, int>
    {
        private Dictionary<Nominal, int> _dict = new Dictionary<Nominal, int> {
            { Nominal.Cents, 0},
            { Nominal.Fives, 0},
            { Nominal.Halfs, 0},
            { Nominal.Quarters, 0}
        };


        public int this[Nominal key]
        {
            get
            {
                return _dict[key];
            }
            set
            {
                _dict[key] = value;
            }
        }
    }
}
