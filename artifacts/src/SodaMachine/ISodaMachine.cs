﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SodaMachine
{
    public interface ISodaMachine
    {
        void PushCoin(Nominal coin);

        void ReturnEscrow(Wallet wallet);

        Wallet BuySoda(); 
    }
}
