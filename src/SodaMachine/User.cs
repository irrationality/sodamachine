﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SodaMachine
{
    public class User 
    {
        public Wallet Wallet { get; set; } = new Wallet();

        public ISodaMachine SodaMachine { get; set; }

        /// <summary>
        /// Пололжить монетку в машину
        /// </summary>        
        public string PushCoin(Nominal coin)
        {
            if (Wallet[coin] == 0) return string.Format("Ooops! You have no {0}", coin);

            Wallet[coin] -= 1;
            SodaMachine.PushCoin(coin);

            return string.Format("You've pushed {0}", coin);
        }

        /// <summary>
        /// Нажать кнопку купить содовую 
        /// </summary>        
        public string BuySoda()
        {
            try
            {
                var change = SodaMachine.BuySoda();
                change.Flush(Wallet,false);
                return string.Format("Wheeee!!! Sodaaaa!!!! your change 1 = {0}, 5 = {1}, 25 = {2}, 50 = {3}",
                   change[Nominal.Cents],
                   change[Nominal.Fives],
                   change[Nominal.Quarters],
                   change[Nominal.Halfs]);
            }
            catch (Exception ex) {

                return string.Format("Oh no! SodaMachine says - {0}", ex.Message);
            }
        }

        /// <summary>
        /// Вернуть положенные монетки        
        /// </summary>        
        public string ReturnEscrow() {
            SodaMachine.ReturnEscrow(Wallet);
            return "";
        }
        
    }
}
