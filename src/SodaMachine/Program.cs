﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SodaMachine
{
    public class Program
    {
        public void Main(string[] args)
        {
            var user = new User { SodaMachine = new SodaMachine()};

            user.Wallet[Nominal.Cents] = 100;
            user.Wallet[Nominal.Fives] = 50;
            user.Wallet[Nominal.Quarters] = 25;
            user.Wallet[Nominal.Halfs] = 12;

            ConsoleKeyInfo key;   

            do
            {
                Console.WriteLine("User coins 1 = {0}, 5 = {1}, 25 = {2}, 50 = {3}",
                    user.Wallet[Nominal.Cents],
                    user.Wallet[Nominal.Fives],
                    user.Wallet[Nominal.Quarters],
                    user.Wallet[Nominal.Halfs]);
                Console.WriteLine();

                Console.WriteLine("SodaMachine coins 1 = {0}, 5 = {1}, 25 = {2}, 50 = {3}",
                    user.SodaMachine.Wallet[Nominal.Cents],
                    user.SodaMachine.Wallet[Nominal.Fives],
                    user.SodaMachine.Wallet[Nominal.Quarters],
                    user.SodaMachine.Wallet[Nominal.Halfs]);

                Console.WriteLine("SodaMachine escrow coins 1 = {0}, 5 = {1}, 25 = {2}, 50 = {3}",
                   user.SodaMachine.Escrow[Nominal.Cents],
                   user.SodaMachine.Escrow[Nominal.Fives],
                   user.SodaMachine.Escrow[Nominal.Quarters],
                   user.SodaMachine.Escrow[Nominal.Halfs]);

                Console.WriteLine();
                Console.WriteLine("1. Push cent");
                Console.WriteLine("2. Push five");
                Console.WriteLine("3. Push quarter");
                Console.WriteLine("4. Push Half");
                Console.WriteLine("5. Buy soda");
                Console.WriteLine("6. Return escrow");

                key = Console.ReadKey();
                Console.WriteLine();
                Console.WriteLine();

                ApplyCommand(key, user);
                Console.WriteLine();
            } while (key.Key != ConsoleKey.Escape);
        }


        public void ApplyCommand(ConsoleKeyInfo key, User user)
        {
            switch (key.Key) {
                case ConsoleKey.D1:
                    Console.WriteLine(user.PushCoin(Nominal.Cents));
                    break;
                case ConsoleKey.D2:
                    Console.WriteLine(user.PushCoin(Nominal.Fives));
                    break;
                case ConsoleKey.D3:
                    Console.WriteLine(user.PushCoin(Nominal.Quarters));
                    break;
                case ConsoleKey.D4:
                    Console.WriteLine(user.PushCoin(Nominal.Halfs));
                    break;
                case ConsoleKey.D5:
                    Console.WriteLine(user.BuySoda());
                    break;
                case ConsoleKey.D6:
                    Console.WriteLine(user.ReturnEscrow());
                    break;
            }
        }
    }
}
