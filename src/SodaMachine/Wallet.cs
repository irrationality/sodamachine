﻿using System;
using System.Collections.Generic;

namespace SodaMachine
{
    //класс кошелек, но по сути словарь номинал кол-во
    public class Wallet
    {
        private Dictionary<Nominal, int> _dict = new Dictionary<Nominal, int> {
            { Nominal.Cents, 0},
            { Nominal.Fives, 0},
            { Nominal.Halfs, 0},
            { Nominal.Quarters, 0}
        };

        // индексатор по номиналу
        public int this[Nominal key]
        {
            get
            {
                return _dict[key];
            }
            set
            {
                _dict[key] = value;
            }
        }

        // "высыпать" все монеты в другой кошелек        
        public void Flush(Wallet wallet, bool leaveEmpty = true)
        {
            wallet[Nominal.Cents] += this[Nominal.Cents];
            wallet[Nominal.Fives] += this[Nominal.Fives];
            wallet[Nominal.Quarters] += this[Nominal.Quarters];
            wallet[Nominal.Halfs] += this[Nominal.Halfs];

            if (leaveEmpty) {
                this[Nominal.Cents] = 0;
                this[Nominal.Fives] = 0;
                this[Nominal.Quarters] = 0;
                this[Nominal.Halfs] = 0;
            }
            
        }
    }
}
