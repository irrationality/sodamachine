﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SodaMachine
{
    public interface ISodaMachine
    {

        // Добавил в интерфейс для вывода в консоль текущих значений монеток в машине 
        Wallet Wallet { get;  }
        Wallet Escrow { get;  }
        
        /// <summary>
        /// Положить монетку в машину
        /// </summary>        
        void PushCoin(Nominal coin);

        /// <summary>
        /// Вернуть положенные монетки 
        /// </summary>        
        void ReturnEscrow(Wallet wallet);


        /// <summary>
        /// Купить содовую 
        /// </summary>
        /// <returns>
        /// Возвращает сдачу 
        /// </returns>
        Wallet BuySoda(); 
    }
}
