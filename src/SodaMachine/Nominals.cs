﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SodaMachine
{
    public enum Nominal
    {
        Cents = 1,
        Fives =  5,
        Quarters=  25,
        Halfs =  50
    }
}
