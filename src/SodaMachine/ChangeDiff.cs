﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SodaMachine
{
    //Служебная структура для подсчета сдачи
    public class ChangeDiff : IComparable<ChangeDiff>
    {
        public Nominal Nominal { get; set; }
        public int Diff { get; set; }

        public int CompareTo(ChangeDiff other) {
            return Diff - other.Diff;
        }
    }
}
