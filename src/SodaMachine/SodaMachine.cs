﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SodaMachine
{
    public class SodaMachine : ISodaMachine
    {
        private const int SodaPrice = 30;
        public Wallet Wallet { get; private set; } = new Wallet();
        public Wallet Escrow { get; private set; } = new Wallet();
        private int EscrowSum { get; set; }

        public void PushCoin(Nominal coin)
        {
            Escrow[coin] += 1;            
            EscrowSum += (int)coin;
        }
         
        public void ReturnEscrow(Wallet wallet)
        {
            Escrow.Flush(wallet);
            EscrowSum = 0;            
        }

        public Wallet BuySoda()
        {
            if (EscrowSum < SodaPrice)
                throw new Exception("You have not enough balance for buying soda");

            var allCoins = new Wallet();
            Wallet.Flush(allCoins, false);
            Escrow.Flush(allCoins,false);

            var change = CountChange(allCoins);

            Escrow.Flush(Wallet);            
            EscrowSum = 0;

            Wallet = allCoins;
            return change;
        }

        private Wallet CountChange(Wallet allCoins)
        {
            var changeSum = EscrowSum - SodaPrice;
            return CountChangeRecursive(changeSum, new Wallet(),allCoins) ;
        }

        private Wallet CountChangeRecursive(int changeSum, Wallet change, Wallet allCoins) {
            if (changeSum == 0) return change;

            var res = Enum.GetValues(typeof(Nominal)).
                Cast<Nominal>()
                .Select(v=> {
                    var diff = changeSum - (int)v;
                    return diff >= 0 && allCoins[v] != 0 ? 
                    new ChangeDiff{ Nominal = v, Diff = diff } :
                    new ChangeDiff{ Nominal = v, Diff = int.MaxValue };
                }).Min();

            if (res.Diff == int.MaxValue)
                throw new Exception("Oh sorry! But I can't give you change");

            allCoins[res.Nominal] -= 1;
            change[res.Nominal] += 1;
            
           

            return CountChangeRecursive(res.Diff, change, allCoins);
            
        }

        private void FlushEscrow(Wallet wallet)
        {
            
            wallet[Nominal.Cents] += Escrow[Nominal.Cents];
            wallet[Nominal.Fives] += Escrow[Nominal.Fives];
            wallet[Nominal.Quarters] += Escrow[Nominal.Quarters];
            wallet[Nominal.Halfs] += Escrow[Nominal.Halfs];           
        }
    }
}
